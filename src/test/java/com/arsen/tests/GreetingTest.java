package com.arsen.tests;

import com.arsen.src.NewYearGreeting;
import com.arsen.src.XMasGreeting;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class GreetingTest {
    private static NewYearGreeting newYearGreeting;
    private static XMasGreeting xMasGreeting;

    @BeforeClass
    public static void initHelloWorld() {
        newYearGreeting = new NewYearGreeting();
        xMasGreeting = new XMasGreeting();
    }

    @Test
    public void testNewYearGetGreeting() {
        assertEquals("Happy New Year!", newYearGreeting.getGreeting());
    }

    @Test
    public void testXMasGetGreeting() {
        assertEquals("Merry XMas!", xMasGreeting.getGreeting());
    }

    @Test
    public void testNewYearMakeGreeting() {
        assertEquals("Happy New Year! Dear: Name!", newYearGreeting.makeGreeting("Name"));
    }

    @Test
    public void testXMasGreeting() {
        assertEquals("Merry XMas! To everyone from Name!", xMasGreeting.makeGreeting("Name"));
    }

}
