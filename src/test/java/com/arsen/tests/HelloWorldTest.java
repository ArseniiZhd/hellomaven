package com.arsen.tests;
import com.arsen.src.HelloWorld;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class HelloWorldTest {
    private static HelloWorld default_helloworld;
    private static HelloWorld not_default_helloworld;

    @BeforeClass
    public static void initHelloWorld() {
        default_helloworld = new HelloWorld();
        not_default_helloworld = new HelloWorld("not default");
    }

    @Test
    public void testDefaultName() {
        assertEquals("Hello World!", default_helloworld.getName());
    }

    @Test
    public void testNotDefaultName() {
        assertEquals("not default", not_default_helloworld.getName());
    }

    @Test
    public void testSetName() {
        default_helloworld.setName("set name of default");
        not_default_helloworld.setName("set name of not default");
        assertEquals("set name of default", default_helloworld.getName());
        assertEquals("set name of not default", not_default_helloworld.getName());
    }
}
