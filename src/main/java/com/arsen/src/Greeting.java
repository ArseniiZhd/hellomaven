package com.arsen.src;


public abstract class Greeting {
    private String Greeting;


    public String makeGreeting(String name) {
        return this.Greeting + " Dear: " + name + "!";

    }

    public void setGreeting(String greeting) {
        Greeting = greeting;
    }

    public String getGreeting() {

        return Greeting;
    }
}
