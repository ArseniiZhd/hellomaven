package com.arsen.src;

public class HelloWorld {
    private String name;

    public HelloWorld() {
        this.name = "Hello World!";
    }

    public HelloWorld(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
