package com.arsen.src;

public class XMasGreeting extends Greeting {
    public XMasGreeting() {
        super.setGreeting("Merry XMas!");
    }

    @Override
    public String makeGreeting(String name) {
        return super.getGreeting() + " To everyone from " + name + "!";
    }
}
